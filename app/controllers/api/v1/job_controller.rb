class Api::V1::JobController < ApplicationController
    before_action :authenticate_user!
    # ALL job application (HOME)
    def index
        @current_date = (Time.now).strftime("%Y-%m-%d")
        @all_job_posts = Job.where("DATE(expiry_date) >= ?", @current_date).all
        if @all_job_posts.present?
            render json:  @all_job_posts
        else
            render json: 'No Job posts yet'
        end        
    end

    # VIEW job application
    def show
        @single_job_post = Job.find(job_params[:id])
        render json: @single_job_post
    end

    private

    def job_params
        params.permit(:id, :title, :description)
    end
    
    
end