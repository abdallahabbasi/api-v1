
class Api::V1::JobApplicationController < ApplicationController
    before_action :authenticate_user!

    def index
        render json: "You are not Admin"
    end

    def show
        render json: "You are not Admin"
    end
    
    # ADD NEW job application
    def create
        @current_user = current_user.id
        @new_job_app = JobApplication.create(job_id: job_application_params[:job_id], user_id: @current_user)
        if @new_job_app.errors.present?
            render json: @new_job_app.errors
        else
            render json: "Job Applicaion is added successfully! "
        end   
    end

    private

        def job_application_params
            params.permit(:id, :job_id, :status, :user_id)
        end
    
end