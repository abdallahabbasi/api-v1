class Api::V1::Admin::JobController < ApplicationController
    before_action :authenticate_user!
    before_action :check_user_type
    # ALL job application (HOME)
    def index        
        @all_job_posts = Job.all        
        render json:  @all_job_posts               
    end

    # VIEW job application
    def show
        @single_job_post = Job.find(job_params[:id])
        render json: @single_job_post
    end

    # ADD NEW job application
    def create
        @current_user = current_user.id
        @new_job = Job.create(user_id: @current_user,title: job_params[:title], description: job_params[:description], expiry_date: job_params[:expiry_date])
        if @new_job.errors.present?
            render json: @new_job.errors
        else
            render json: "Post is added successfully! "
        end        
    end

    # UPDATE job application
    def update
        @update_job  = Job.where(id: job_params[:id]).update(title: job_params[:title], description: job_params[:description], expiry_date: job_params[:expiry_date])
        # if @update_job.errors.present?
        #     render json: @update_job.errors
        # else
            render json: "Post is updated successfully! "
        # end        
    end
    
    # DELETE job application
    def destroy
        @delete_job  = Job.find(job_params[:id]).destroy
        if @delete_job.errors.present?
            render json: @delete_job.errors
        else
            render json: "Post is Deleted successfully! "
        end
    end

    private

        def job_params
            params.permit(:id, :title, :description, :user_id, :expiry_date)
        end

        def check_user_type
            if user_signed_in?
                unless (current_user.role == 'admin')
                    render json: 'You dont have permesion'
                end
            end
        end
    
end