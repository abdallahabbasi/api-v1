
class Api::V1::Admin::JobApplicationController < ApplicationController  
    before_action :authenticate_user!  
    before_action :check_user_type
    # ALL job application (HOME)
    def index
        @all_job_app= JobApplication.all
        render json: @all_job_app
    end

    # VIEW job application
    def show
        JobApplication.where(id: job_application_params[:id]).update(status: 1)
        @single_job_app = JobApplication.find(job_application_params[:id])
        render json: @single_job_app
    end

    # ADD NEW job application
    def create
        @current_user = current_user.id
        @new_job_app = JobApplication.create(job_id: job_application_params[:job_id], user_id: @current_user)
        if @new_job_app.errors.present?
            render json: @new_job_app.errors
        else
            render json: "Job Applicaion is added successfully! "
        end   
    end

    # UPDATE job application
    def update
        @update_job_app  = JobApplication.where(id: job_application_params[:id]).update(job_id: job_application_params[:job_id], status: job_application_params[:status])
        # if @update_job_app.errors.present?
        #     render json: @update_job_app.errors
        # else
            render json: "Job Applicaion is updated successfully! "
        # end  
    end

    # DELETE job application
    def destroy
        @delete_job_app  = JobApplication.find(job_application_params[:id]).destroy
        # if @delete_job_app.errors.present?
        #     render json: @delete_job_app.errors
        # else
            render json: "Job Applicaion is Deleted successfully! "
        # end
    end

    private

        def job_application_params
            params.permit(:id, :job_id, :status, :user_id)
        end

        def check_user_type
            if user_signed_in?
                unless (current_user.role == 'admin')
                    render json: 'You dont have permesion'
                end
            end
        end
    
end