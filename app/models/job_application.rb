class JobApplication < ApplicationRecord
    validates :job_id, presence: true
    validates :user_id, presence: true
    validates :status, presence: true

    enum status: { not_seen: 0 , seen: 1 }

    belongs_to :user
    has_one :job
end
