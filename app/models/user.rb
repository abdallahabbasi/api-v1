class User < ApplicationRecord
        # Include default devise modules. Others available are:
  
        devise :database_authenticatable, :registerable,
                :recoverable, :rememberable, :validatable
                # ,:confirmable, :omniauthable, :trackable
                # ,:lockable, :timeoutable
        include DeviseTokenAuth::Concerns::User

        enum role: { admin: 0 , user: 1 }

        has_many :job_applications
        has_many :jobs

        
end
