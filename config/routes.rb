Rails.application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'api/v1'
  namespace :api do
    namespace :v1 do
      namespace :admin do
        resources :job_application
        resources :job
      end
      resources :job
      resources :job_application
    end
   end
end
  