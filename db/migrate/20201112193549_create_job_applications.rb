class CreateJobApplications < ActiveRecord::Migration[6.0]
  def change
    create_table :job_applications do |t|
      t.integer :job_id
      t.string :seeker_name
      t.text :seeker_details
      t.integer :status, :null => false, :default => 0

      t.timestamps
    end
  end
end
