class RemoveSeekerNameFromJobApplication < ActiveRecord::Migration[6.0]
  def change
    remove_column :job_applications, :seeker_name
    remove_column :job_applications, :seeker_details
    add_column :job_applications, :user_id, :integer
    add_column :jobs, :user_id, :integer
  end
end
