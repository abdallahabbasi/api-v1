class AddExpiryDateToJobs < ActiveRecord::Migration[6.0]
  def change
    add_column :jobs, :expiry_date, :date
  end
end
